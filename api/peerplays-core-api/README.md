# Peerplays Core API

{% content-ref url="popular-api-calls.md" %}
[popular-api-calls.md](popular-api-calls.md)
{% endcontent-ref %}

{% content-ref url="account-history-api.md" %}
[account-history-api.md](account-history-api.md)
{% endcontent-ref %}

{% content-ref url="asset-api.md" %}
[asset-api.md](asset-api.md)
{% endcontent-ref %}

{% content-ref url="block-api.md" %}
[block-api.md](block-api.md)
{% endcontent-ref %}

{% content-ref url="crypto-api.md" %}
[crypto-api.md](crypto-api.md)
{% endcontent-ref %}

{% content-ref url="database-api.md" %}
[database-api.md](database-api.md)
{% endcontent-ref %}

{% content-ref url="network-broadcast-api.md" %}
[network-broadcast-api.md](network-broadcast-api.md)
{% endcontent-ref %}

{% content-ref url="network-nodes-api.md" %}
[network-nodes-api.md](network-nodes-api.md)
{% endcontent-ref %}

{% content-ref url="orders-api.md" %}
[orders-api.md](orders-api.md)
{% endcontent-ref %}

