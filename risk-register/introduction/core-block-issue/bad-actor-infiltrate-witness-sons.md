# Bad Actor infiltrate witness/SONs

## Description

Bad actors can be a threat to any organization. As they infiltrate and gather confidential information about the company. Those action must be considered as critical and proper action must be taken to avoid the risk. The following points can help the user to have some idea about how to overcome this problem.

## Mitigation Ideas

1. Follow a robust on-boarding and vetting procedure, while accepting a witnesses/SONs.
2. Witnesses should be able to keep their anonymity.
3. Implement proof of pulse consensus.
4. Ensure setup of witnesses is simple to lower barrier to entry.
5. Encourage active voting and participation.
6. Publish witness stats in central location periodically.
7. Use a White hat to identify the vulnerabilities in any account with user's consent. (White hat -Ethical security hacker)

## Response Plan

1. Deeper monitoring of chain activity.
2. Push notification for any large transaction (Whale alert)
3. Establish a  "Forensic Team" that can recognize any malicious activities of a witness.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

There are two members who can inspect the issue,

1. Log-Diving analyst
2. Forensics Team

### Second Responder Team

The voters can inform about the invaders, if any.
