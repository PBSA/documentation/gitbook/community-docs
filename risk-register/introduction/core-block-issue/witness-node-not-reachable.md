# Witness node - Not reachable

## Description

The witness node can be unreachable at times due to several reasons. In order to overcome this situation the following ideas and plans can be helpful.

## Mitigation Ideas

1. Create a witness chat room with group call functionality.&#x20;
2. The witnesses should keep note of cell numbers of other witnesses as much as possible.
3. At least, one group where all the witness/ SONs should reside.
4. Create a separate portion for Witnesses in Scenes
5. A Dashboard to track the status of witnesses and chains.

## Response Plan

1. Can we create a witness buddy system
2. Cross-post calls for action - Rocket chat, Telegram, Scenes, etc.,
3. Get familiar with common hubs used by witnesses

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

Firstly, the **Witnesses** themselves have to communicate about the problem with the team. So that, the issue will be know to the team and solution can be provided at the earliest.
