---
description: Distributed Denial-of-service (DDOS)
---

# Witnesses get DDOS'd

## Description

In some cases, the witnesses themselves will be DDOS'd and will be denied access to use the application. This is critical and the following plans, ideas will help in sorting the issue to provide some solution.

## Mitigation Ideas

1. Witnesses should have a backup or DDOS protection.
2. Maintain backup node which can monitor and do automatic failover.
3. Create a basic witnesses node that can take over automatically at once the public witnesses fail.
4. Create witness backup in different location.
5. Any temporary Fallback strategy for witness outage??
6. Any Recommendation to witnesses about some reliable DDOS protection service ??

## Response Plan

1. Outline proper procedure to be taken during the event of DDOS attack.
2. Allocate individual resources to communicate periodically with witnesses about the protective measures and procedure to prevent DDOS attack.
3. Can vote out a witnesses who is been attacked, until they are able to get back on track
4. If attack is persistent, have some IAC (Infrastructure as code) to redeploy the witnesses elsewhere.
5. In case, if any witnesses missing blocks in a row, then Notification should pop-up.
6. Automated system messages in witness chat.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

The Witness / party that notices the issue.

### Second Responder Team

The witness recovery - Liasson
