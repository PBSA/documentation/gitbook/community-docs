# Expensive Servers

## Description

The network run the servers which can become expensive for the user based on the demands, usage and availability. In this case, to maintain the users the following plans and ideas can be useful. Also, the point of contact to understand the issue is listed in this section.

## Mitigation Ideas

1. Increase liquidity to bring value to the token.
2. Run a node with less demand, Find a way for it.
3. To avoid compilation, provide the binaries of witness\_node and cli\_wallet.
4. Create best practices to build cost effective nodes
5. Have Periodic sync-up with other witnesses to learn about the ways remain economical.
6. Static-linking of libbitcoin
7. Publish minimum requirements to witnesses to maintain the standard spec across the platform.
8. Provide the Personal Package Archives (PPA) on ubuntu.&#x20;

## Response Plan

1. Increase witness / SON pay

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

**Point of Contact:** DOCS Team

### Second Responder Team

**Point of Contact:** Dev Team (For Static Link / PPA)
