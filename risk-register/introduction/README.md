# Introduction

The Risk register document helps the user to have an understanding about the risk that has occurred/ might occur in Peerplays. The document focus on the top 3 risks that are voted by the SMEs, categorized as High risk and also provides the possible solutions to solve the risk.

The document has the section which features the Responder Team which is vital to inform about the issue and to find the solution to solve the issue.

The inputs are gathered from the SMEs and categorized as mentioned below,

1. Bunker Issue
2. Core-Block Issue
3. Credential Security
