---
description: Distributed Denial-of-Service (DDOS)
---

# DDOS Attack

## Description

DDOS attack will deny the User to connect with any online services, sites, and application. This is a cyber attack and it's unpredictable. If a user come across any such circumstances, the below ideas, response plan can be helpful.&#x20;

## Mitigation Ideas

The following ideas are listed based on the inputs from SMEs

1. Having Load balancers in place.
2. A method to detect DDOS attack while they originate.
3. Using services like **CloudFlare** to secure the internet connectivity.
4. Limit the number of connections (Reasonable counts).
5. Direct communication with ISP NOCs.
6. Keeping the Backups servers Off-premises .
7. Find a good counter-measure software to stop/fight against the attack.

## Response Plan

1. If attack has happened, the servers can be flipped to backups.
2. Communicate with marketing team depending on the services attacked.

## Responder Team

In order to find the solution / to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

Devops Team\
**Point of contact:** Haxhi&#x20;

### Second Responder Team

Development Team

