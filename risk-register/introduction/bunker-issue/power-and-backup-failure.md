# Power and Backup Failure

## Description

Power is the key factor to run any server. There are situation where the power and backup can fail to switch back which halt the servers to come up. It can be any disaster, natural calamities, accidents, etc., In order to overcome this situation, the following ideas and plans were proposed by the SMEs. This can help the user to understand the possibilities and person to help in retrieve the situation to normal.

## Mitigation Ideas

1. Providing a backup to backups.
2. Plan Tier-3 availability.
3. Generators for critical operations with auto-on features over power failure.
4. Key actors need to have clear communication among themselves
5. The mirrored servers can be Off-premises
6. Maintenance Team with 24/7 availability

## Response Plan

1. Re-deploy the services such as AWS, Linode, etc., whenever in need
2. Pre-written redeploy plans for critical services
3. Start the Generator

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

**Point of Contact:** Jonathan / Maintenance Team

### Second Responder Team

**Point of Contact:** Kyle and Riley respectively
