# Crypto Restriction

## Description

There are possibilities that Government can restrict/ban crypto in the country or in any particular province. If that is the situation, the following plans helps in overcoming the problem. Also, provide the responder team who can clarify the status of that condition and possible solutions.&#x20;

## Mitigation Ideas

1. Always having a proactive design to avoid the regulatory compliance&#x20;
2. A separate role to monitor the regulatory trends
3. Having a duplicate setup in another country
4. Lobby the Govt to have a Crypto friendly policy
5. Stay away from promoting ideas that could cause regulation issue
6. Have a legal counsel
7. Anonymous users as witnesses

## Response Plan

1. Create Server space to Offload several services in other countries with Jurisdictional ability.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

**Point of contact:** Jonathan / Leadership Team

### Second Responder Team

**Point of contact:** Team B
