# Bunker Issue

The entire Peerplays network works in a Bunker. There are several things that might bring down the network. This document covers the possible risks along with the plan to solve any such occurrences. Though there are several possible risks, only 4 risks are filtered based on the audience (SME's) choice.

The list of Risks discussed are

1. DDOS
2. Hypervisor Compromised
3. Power and backup Failure
4. Crypto Restriction

