# Hypervisor Compromised

## Description

If the attacker has the control over the hypervisor, then the full access to VMs will be under threat. In order to overcome this situation, the following plans and ideas can be helpful. The document outcome is the ideas discussed among the SMEs.

## Mitigation Ideas

1. Limit the access to admin
2. PEN Testing the accounts to evaluate the security. Example: Brute force test on password.
3. Having Backups of Hypervisor and nodes for Fallback/re-deployment
4. Performing Audits periodically&#x20;

## Response Plan

1. Updated backups that can be written over the compromised systems.
2. A Kill-switch to stop the process.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

**Point of Contact:** Rily and/or Robert

### Second Responder Team

**Point of Contact:** Kyle and Haxhi
