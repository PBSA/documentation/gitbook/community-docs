# SONs active keys exposed in configuration

## Description

If there is a possibilities that active keys getting exposed in the configuration, then the following ideas can be considered as a solution.

## Mitigation Ideas

1. QA process to detect any key exposure
2. Proper code review has to be done before code merge to avoid conflict.
3. Process in place to cycle active keys.
4. Using active key of Operator account on server instead of SON account active key - Whether _custom\_permission_ can be used to limit the damage caused by any Key?
5. Can we make config files encrypted which can be accessed only via their own passphrase?
6. Is it possible to write SONs code not to be dependent on the active key ?
7. Any possibility to get certain config value comes from ENV variable?

## Response Plan

1. Shut down SON, change the active key.
2. Vote out SON
3. Use Owner key to change the active key.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

**Point of Contact:** SON operator

### Second Responder Team

**Point of Contact:** The community, who can vote out SON
