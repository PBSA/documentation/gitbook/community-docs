# NEX Open Source Vulnerabilities

## Description

NEX is an open source application which might paves way for any security threat. So, this section helps the user to have some ideas to overcome the issue.&#x20;

## Mitigation Ideas

1. Maintain a list of "endorsed" deployments (URL list) to capture any unauthorized deployment.
2. Audit all the code including smart contracts, sites regularly.
3. PEN Test run in the production environment related to NEX

## Response Plan

1. Alert message the community about the invaders.
2. Any scope to build API nodes that can blacklist specific services.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

If any suspicious activities are observed, the observer can inform about it through any User forum. All the users must be informed about it.&#x20;

### Second Responder Team

**Point of contact:** DEV OPS, NEX devs
