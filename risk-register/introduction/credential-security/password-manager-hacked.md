# Password manager hacked

## Description

A strong password is always recommended to keep the accounts safe from hackers. Still, there are many software techniques in the industry to hack the password and use it against the owner. As the world is digitalized, crimes also getting smarter to acquire our wealth. In case, if the passwordr manag is hacked, the following ideas might be helpful in overcoming the situation.

## Mitigation Ideas

1. Have back-up of all stored passwords.
2. Keep the high-profile passwords securely and don't save them in the password manager.
3. If cloud services are in use, then use self hosted backup.
4. Use a company hosted password manager (Bitwarden)and force 2FA as one of the factor.
5. User should use 2FA wherever possible.&#x20;

## Response Plan

1. Plan to change the password periodically.
2. Inform all employee who will be affected by this disruption.
3. Use back up to reset passwords of all account.

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

The employee whose credential has be hacked, should first inform the details to the Team. So that,  the action to stop the intruder can be taken care.

**Point of contact:** Rily

### Second Responder Team

**Point of contact:**

1. DevOps
2. System Admin
