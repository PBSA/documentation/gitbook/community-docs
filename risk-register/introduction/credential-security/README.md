# Credential Security

The risks discussed in this document are,

1. NEX Open source Vulnerabilities
2. Key Logger, access to NEX deployment
3. Password manager hacked
4. SONs active keys exposed in configuration
