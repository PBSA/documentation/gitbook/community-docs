# Keyloggers, access to NEX deployment

## Description

Keylogger can also be as source to reveal the User's details with the hacker. A User must be aware of such malicious activities and prevent themselves from losing their information. This section helps the user with ideas and plans from SME's to overcome the situation.

## Mitigation Ideas

1. If the main deployment is compromised, then shutdown & re-deploy immediately.
2. Run anti-virus and anti-keylogger software to scan network devices.
3. Create an anti-phishing system that can capture the malicious activities such as code duplication and deploying in other environment. The User should be know that the code is not from original/official.
4. Isolate the compromised system in locked environment to perform RCA.
5. Monitoring service that polls the deployment and notifies key parties whenever the hosted code changes.

## Response Plan

1. Remove the infected account in the GIT lab settings.
2. Inform all the users and employees that will be impacted by the attack.
3. Perform a rapid DNS switchover to site with "Down for Maintenance" status until deployment is recovered.
4. Chain analysis to find the number of affected users.
5. Contact GitLab for help.
6. Any possibilities to have a counter-cyber crime employee to hack the hackers?
7. Article about the hacker attack,\
   [https://decrypt.co/108015/nears-rainbow-bridge-blocks-another-attack-costing-hackers-5-ethereum](https://decrypt.co/108015/nears-rainbow-bridge-blocks-another-attack-costing-hackers-5-ethereum)

## Responder Team

In order to find the solution/ to know in details about the risk that has happened, responder team is one to be communicated first.&#x20;

### First Responder Team

**Point of Contact:** DEV or Network Ops

### Second Responder Team

**Point of Contact:** Community Outreach though any User forum
