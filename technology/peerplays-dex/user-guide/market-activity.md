# Market Activity

The market activity page helps in analysing the statistics of any trading pair. The statistics include the value of current price, change (in percentage), volume for any trading pair.

The trading pair will be between the following assets,

* BTC
* HBD
* HIVE
* PPY

The market page navigation can be done in two ways,

1. **From the Dashboard**

Click on the **Market Tab** in the Dashboard page and select any Trading pair to navigate to the Market page.

<figure><img src="../../../.gitbook/assets/Dashboard-marketpage-copy.JPG" alt=""><figcaption><p>Fig-1: Dashboard selection</p></figcaption></figure>

&#x20; 2\. **From the Menu option**

On the home page, click on the three dots present in the right pane. All the options available will be listed and select Market from the available options to navigate to the Market tab.

<figure><img src="../../../.gitbook/assets/Market-other-option.jpg" alt=""><figcaption><p>Fig-2: Menu option-Market page</p></figcaption></figure>

### 1. Market page Overview

The market page consists of the following sections,

* Trading pair selection
* Buy and Sell asset
* My Open orders
* My Open History



<figure><img src="../../../.gitbook/assets/Marketpage-overview.JPG" alt=""><figcaption><p>Fig-3: Market page </p></figcaption></figure>

### 1. Trading pair selection

To select any trading pair, click on the drop-down button available on the left pane of the market page. The drop-down option opens a tab to select the desired asset for trading.

<figure><img src="../../../.gitbook/assets/Market-page (1).JPG" alt=""><figcaption><p>Fig-4: Trading pair selection</p></figcaption></figure>

The select pair tab will allow the user to choose any assets like BTC, HBD, HIVE, and PPY. The pairing can be done among these assets.&#x20;

In the recent pair option, the existing selection will be displayed. The user can choose the trading pair from this button too.

Finally, click on confirm to choose the trading pair or choose to cancel to go back to the market page.

<figure><img src="../../../.gitbook/assets/Marketpage-tradingpair-selection.JPG" alt=""><figcaption><p>Fig-5: Select pair</p></figcaption></figure>

For any trading pair, the details about the pair will be listed like current price, change and volume. There are two options to analyse the selected trading pair namely,

* Order Book
* Trade history and My History

#### Order Book

This option provides details about the total, sell, and buy orders based on user selection from the options available.

It also has a drop-down list to choose the threshold value.

<figure><img src="../../../.gitbook/assets/MP-order-book.JPG" alt=""><figcaption><p>Fig-6: Asset summary</p></figcaption></figure>

#### History

The history option helps the user to analyse the previous activities under the selected trading pair. It shows the details about the buy and selling details of asset with price and time.

Red color represents the sell orders while green color is for buy orders.

<figure><img src="../../../.gitbook/assets/MP-Trade-history.jpg" alt=""><figcaption><p>Fig-7: Trade history</p></figcaption></figure>

### 2. Buy and Sell Asset

This section on the Market page helps to Buy/sell the asset of the selected Trading pair. The assets will change according to the pair selection. In this example, the selected trading pair is BTC/PPY.

Input the value for the Price and quantity of an asset to calculate the Total value. Based on the input, the fees, market fees, and balance will be updated. Click on the Buy/Sell button to place the order.

<figure><img src="../../../.gitbook/assets/MP-Buy-asset-option.jpg" alt=""><figcaption><p>Fig-8: Buy  Asset</p></figcaption></figure>

The user has to click on the "sell" tab to sell the asset pair. The below diagram explains the options available to select when selling the asset.

<figure><img src="../../../.gitbook/assets/MP-sell-option.JPG" alt=""><figcaption><p>Fig-9: Sell asset</p></figcaption></figure>

### 3. My Open Orders

The open orders tab displays the current order details for the selected trading pair. The details include price, asset value (in this case, BTC and PPY), expiration, and action.

<figure><img src="../../../.gitbook/assets/MP-my-open-orders.jpg" alt=""><figcaption><p>Fig-10: My open order</p></figcaption></figure>

### 4. My Order History

In this section, the order history of the selected trading pair will be displayed. The values such as asset (In this example BTC and PPY), price and date of purchase are listed.

<figure><img src="../../../.gitbook/assets/MP-my-order-history (1).JPG" alt=""><figcaption><p>Fig-11: My open history</p></figcaption></figure>

## 5. Asset

The asset option on the market page has two options "Deposit" and "Withdraw"  which on the click navigates to the wallet page for receiving and sending assets respectively.&#x20;

<figure><img src="../../../.gitbook/assets/MP-Assets-option.jpg" alt=""><figcaption><p>Fig-12: Assets tab to navigate to wallet page </p></figcaption></figure>

