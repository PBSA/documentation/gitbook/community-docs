---
description: An example to get the bitcoin address
---

# Bitcoin Transaction

Depending on the wallet that you’re using, you may have to use a tool like [bitaddress.org](https://www.bitaddress.org/) to find your Public Key if it’s not already displayed in your wallet.

Wasabi Wallet, for example, displays your Public key.&#x20;

But wallets like [Trust Wallet](https://growfollowing.com/trust-wallet-private-key/) and Exodus require you to use external tools like [bitaddress.org](https://www.bitaddress.org/) to find your Public Key.

In this example, we’ll be using Wasabi Wallet as the Deposit address and Exodus as the withdrawal address.

As you can see in the image below, the Deposit Address (Wasabi Wallet) is bc1qhlu97p4ehnvt2274na5xqra5n8a8cnumavatn3

The Deposit Public Key is 02d5a014ec94974a01b5b81550bbcf6a629715140d38e7da5f1c2c71ccdcd8b61c

<figure><img src="../../../../.gitbook/assets/step4.webp" alt=""><figcaption></figcaption></figure>

The Withdraw Address (Exodus Wallet) is bc1qrk768lztvrjduama2ruuut9eweamtz5ru4vmhl

But we must find the Withdraw Private Key.. Here’s how:

<figure><img src="../../../../.gitbook/assets/step5.webp" alt=""><figcaption></figcaption></figure>

Open your wallet. As seen in the image above, click on the three dotted lines and click “View Private Keys.” Then Click ”Yes, I’m sure.”

<figure><img src="../../../../.gitbook/assets/step6.webp" alt=""><figcaption></figcaption></figure>

Next, type your account password.

<figure><img src="../../../../.gitbook/assets/step7.webp" alt=""><figcaption></figcaption></figure>

And now you’ll see your Bitcoin deposit Address as well as your Private Key. Copy the address that starts with bc1 and it’s Private key.

<figure><img src="../../../../.gitbook/assets/step8.webp" alt=""><figcaption></figcaption></figure>

The Withdraw Private Key is L5896NkkABQ9PCcWHQiM1tAyFWoPtaEf1uLRQ5eG7nTvzXeXY29g

We will now use the Private Key to find out our Public Key. Simply head over to [bitaddress.org](https://www.bitaddress.org/). Then, click “Wallet Details”.

<figure><img src="../../../../.gitbook/assets/step9.webp" alt=""><figcaption></figcaption></figure>

Paste your Private Key and click “View Details.”

<figure><img src="../../../../.gitbook/assets/step10.webp" alt=""><figcaption></figcaption></figure>

You will find your Public Key over here. Make sure you copy and paste the compressed, 66 characters \[0-9A-F] and **NOT** the 130 Characters key.

<figure><img src="../../../../.gitbook/assets/step11.webp" alt=""><figcaption></figcaption></figure>

We now have the Deposit Public Key, the Withdraw Public Key, and the Withdraw Address.&#x20;

**Deposit Public Key (Wasabi Wallet – already displayed):** 02d5a014ec94974a01b5b81550bbcf6a629715140d38e7da5f1c2c71ccdcd8b61c

**Withdraw Public Key (Exodus Wallet – found through bitaddress.org): 038F14B6B61EF0AE7EF8317E757807CA8322257C96D54635979C1FBBB899621AF2**

**Withdraw Address (Exodus Wallet):** bc1qrk768lztvrjduama2ruuut9eweamtz5ru4vmhl
