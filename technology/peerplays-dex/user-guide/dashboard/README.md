---
description: The first screen on the NEX page
---

# Dashboard

There are 4 tabs in the dashboard section,

1. Deposit
2. Withdraw
3. Swap
4. Market

## 1. Deposit

This section helps the user to deposit assets into the account. The current version supports three assets BTC, HIVE, and ETH.

<figure><img src="../../../../.gitbook/assets/Deposit-1.jpg" alt=""><figcaption><p>Fig-1 Deposit option</p></figcaption></figure>

### BTC Deposit

To deposit Bitcoin, select BTC from the drop-down list and click on **Generate Bitcoin address**. This prompts you to enter the password to confirm the validation.&#x20;

<figure><img src="../../../../.gitbook/assets/bitcoin-deposit.JPG" alt=""><figcaption><p>Fig-2: Bitcoin Deposit</p></figcaption></figure>

Please follow the steps in the below link for any Bitcoin transaction,

{% embed url="https://community.peerplays.com/~/changes/8PhtH7T32e0eFNJI7WpX/technology/peerplays-dex-1/user-guide/dashboard/bitcoin-transaction" %}

## HIVE Deposit

To deposit HIVE/HBD, select HIVE from the drop-down menu. It instructs to send funds to son-account on hive blockchain with memo as account name.

<figure><img src="../../../../.gitbook/assets/HBD-deposit.JPG" alt=""><figcaption><p>Fig-3: Hive Deposit</p></figcaption></figure>

### Ethereum Deposit

To deposit Ethereum, first, the user should add the Ethereum deposit address,

<figure><img src="../../../../.gitbook/assets/eth-deposit (1).JPG" alt=""><figcaption><p>Fig-4: Add Ethereum address</p></figcaption></figure>

## 2. Withdraw

To withdraw the desired asset choose withdraw tab in the dashboard. There are two assets supported in this version BTC and HIVE.

## BTC Withdraw

1. Select BTC from the list of options in the drop-down list.
2. Enter the required amount of BTC to be withdrawn.
3. Enter the Compressed withdraw public key & address from the keys text file.
4. The Estimated fee, total transaction, and time will be calculated based on the withdrawal amount.
5. Click on the **Withdraw** button to initiate the transaction.

<figure><img src="../../../../.gitbook/assets/bitcoin-withdraw.JPG" alt=""><figcaption><p>Fig-5: BTC withdraw</p></figcaption></figure>

## HIVE Withdraw

1. Select **HIVE** from the list of options available in the drop-down list
2. Enter the amount to be withdrawn from the account
3. Enter the HIVE account withdrawal address in the text box
4. The Fees, total transaction, and time will be calculated based on the withdrawal amount.
5. Click on **Withdraw** to initiate the transaction.

<figure><img src="../../../../.gitbook/assets/hive-withdraw.JPG" alt=""><figcaption><p>Fig-6: Hive Withdraw</p></figcaption></figure>

## Ethereum Withdraw

1. Select **ETH** from the list of options available in the drop-down list.
2. Enter the amount to be withdrawn from the account.
3. Enter the **ETH account address** withdrawal address in the text box.
4. The Fees, total transaction, and time will be calculated based on the withdrawal amount.
5. Click on **Withdraw** to initiate the transaction.

<figure><img src="../../../../.gitbook/assets/Eth-witdraw-page.JPG" alt=""><figcaption><p>Fig-7: ETH Withdraw</p></figcaption></figure>

## 3. Swap

The swap functionality is a quick way to exchange the asset. The assets that can be exchanged with each other are Bitcoin(BTC), Hive (HBD), and Peerplays (PPY).

<figure><img src="../../../../.gitbook/assets/Swap-asset (2).jpg" alt=""><figcaption><p>Fig-8: Asset options</p></figcaption></figure>

1. Select the asset from which the amount has to be swapped and enter the amount to be transferred.
2. Select the asset to which the amount has to be received. The amount will be calculated based on the transfer amount.
3. The fees and type of transaction will be displayed.

<figure><img src="../../../../.gitbook/assets/swapping-asset.JPG" alt=""><figcaption><p>Fig-9: Coin swap</p></figcaption></figure>

4\. Click on the **Swap Coins** button to initiate the swap which prompts you to enter the password for validation.

5\. The swap order will be displayed. Click on **Confirm** button to complete the swap.

<figure><img src="../../../../.gitbook/assets/swap-order.JPG" alt=""><figcaption><p>Fig-10: Swap order transaction</p></figcaption></figure>

6\. After successful swapping, the success message will be displayed.

<figure><img src="../../../../.gitbook/assets/swap-order-complete.JPG" alt=""><figcaption><p>Fig-11: Transaction confirmation</p></figcaption></figure>

&#x20;7\. Click Done to reflect the changes in the dashboard.

## 4. Market

The Market tab is the shortcut way to choose the Trading pair for any transaction. The trading pairs will be listed in blocks, hover over the blocks to click any option.



<figure><img src="../../../../.gitbook/assets/Dashboard-marketpage.JPG" alt=""><figcaption><p>Fig-12: Market page</p></figcaption></figure>

On clicking the desired block, it will direct to the Market tab to display all the activities of the Trading pair in detail.&#x20;

<figure><img src="../../../../.gitbook/assets/Market-page (1).JPG" alt=""><figcaption><p>Fig-13: Market-page Trading pair</p></figcaption></figure>

The Market page can also be chosen from the list of options available from the menu.

<figure><img src="../../../../.gitbook/assets/Market-other-option.jpg" alt=""><figcaption><p>Fig-14: Menu selection-Market page</p></figcaption></figure>
