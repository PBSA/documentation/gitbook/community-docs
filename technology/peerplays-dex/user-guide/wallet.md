# Wallet

The wallet option allows the user to view the asset available in that account. It also has the option to send and receive assets from other accounts.

The Wallet has three sections,

1. Asset
2. Send
3. Receive

## 1. Asset

This section displays the list of assets available in the account. A search option is available to find any desired asset. The list of assets can be downloaded in the form of a PDF/CSV file.

The list of assets can be filtered based on symbol, name, available tokens, and orders.

Each asset has two buttons namely, send/receive to perform the transaction in a single click.&#x20;

<figure><img src="../../../.gitbook/assets/wallet-page.JPG" alt=""><figcaption><p>Fig-1: Asset details</p></figcaption></figure>

## 2. Send

The send section has the option to send available assets from one wallet to another account.&#x20;

1. Fill in the form with the correct input based on your transaction.

<figure><img src="../../../.gitbook/assets/wallet-send.JPG" alt=""><figcaption><p>Fig-2: Sending asset to account</p></figcaption></figure>

2\. Click on the **Send** button to initiate the transfer. Next, it prompts you to enter the password for validation.

3\. Click on **Confirm** to complete the transaction. The asset will be successfully transferred to another account.

<figure><img src="../../../.gitbook/assets/send-asset-confirm.JPG" alt=""><figcaption><p>Fig-3: Transaction confirmation</p></figcaption></figure>

## 3. Receive

To receive an asset from another account to your account, the wallet receives option can be used. Select the desired option under the **Receive** Assets drop-down list.

Based on the asset selection the hint to perform the transfer will be provided. Follow that to complete the transaction.

### List of assets

<figure><img src="../../../.gitbook/assets/Wallet-receive.jpg" alt=""><figcaption><p>Fig-4: List of asset to receive</p></figcaption></figure>

### &#x20;After asset selection

<figure><img src="../../../.gitbook/assets/wallet-receive-info.JPG" alt=""><figcaption><p>Fig-5: Asset selection to receive</p></figcaption></figure>
