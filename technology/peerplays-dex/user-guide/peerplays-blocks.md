# Peerplays Blocks

The  Peerplays blockchain section helps the user to understand the transactions happening in the blocks. This page has the details of blockchain, assets, witnesses, committees, sons, and fees associated with that account.

To navigate to this page, click on the Blocks from the list of options available in the Menu on the right pane.

<figure><img src="../../../.gitbook/assets/Blocks-option.jpg" alt=""><figcaption><p>Fig-1: Block option in Menu</p></figcaption></figure>

## Blocks Page - Overview

The block page consists of details about the blocks associated with the account. The tabs under this page are Blockchain, Assets, Witnesses, Committees, Sons, and Fees.

<figure><img src="../../../.gitbook/assets/1 (4).JPG" alt=""><figcaption><p>Fig-2: Blockpage Overview</p></figcaption></figure>

## 1. Blockchain

This tab contains information about the block based on the recent activity. The information consists of current block, last irreversible block, confirmation time, supply.

The most recent block activity will be featured on the top of page and it will be updated based on each block activity.

<figure><img src="../../../.gitbook/assets/Showing witness info.JPG" alt=""><figcaption><p>Fig-3: List of blocks</p></figcaption></figure>

Click on a specfic Block ID to learn about the block in detail.

<figure><img src="../../../.gitbook/assets/2 (4).JPG" alt=""><figcaption><p>Fig-4: Block in detail</p></figcaption></figure>

## 2. Asset

The number of assets associated with the account will be listed in numbers. The search bar provides an option to search assets along with the option to download the details in PDF/CSV file format for future reference.

The filter options available to sort the assets are ID, Max supply, and Precision in ascending/descending order. Also, the assets can be sorted based on certain categories using symbol, name, and issuer options.

<figure><img src="../../../.gitbook/assets/3-asset.JPG" alt=""><figcaption><p>Fig-5: Asset selection</p></figcaption></figure>

## 3. Witnesses

The witnesses associated with the account will be listed in blocks. The number of active and current witnesses along with earnings will be listed.

The search bar helps in finding the witness at ease. There is an option to download the list in PDF/CSV format for future reference.

The filter function helps to sort the data based on rank, total votes, last block, and missed block counts.

Click on the name to learn about the witness activity in detail.

<figure><img src="../../../.gitbook/assets/4-witnessess.JPG" alt=""><figcaption><p>Fig-6: List of Witness</p></figcaption></figure>

<figure><img src="../../../.gitbook/assets/Witness-activity.JPG" alt=""><figcaption><p>Fig-7: Asset in detail</p></figcaption></figure>

## 4. Committees

The number of active committees for the account will be displayed in blocks. The search bar helps in finding the member at ease. There is an option to download the list in PDF/CSV format for future reference.&#x20;

The filters will be based on rank, and total votes and can be sorted in ascending/descending order. Click on the name to learn the activity of each committee in detail.

<figure><img src="../../../.gitbook/assets/5-committee.JPG" alt=""><figcaption><p>Fig-8: List t of active Committee </p></figcaption></figure>

The activities of any specific committee member can be monitored/viewed by clicking on the committee name.

<figure><img src="../../../.gitbook/assets/committe-activity.JPG" alt=""><figcaption><p>Fig-9: Committee activity</p></figcaption></figure>

## 5. SONs

The number of active Sons will be displayed in blocks along with the budget and next vote update time.

The search bar helps in finding the account at ease. There is an option to download the list in PDF/CSV format for future reference.

Based on Rank and total votes the list can be filtered. Click on the particular name to learn their activity in detail.

<figure><img src="../../../.gitbook/assets/6-Sons.JPG" alt=""><figcaption><p>Fig-10: SONs detail</p></figcaption></figure>

## 6. Fees

The standard fees associated with each transaction will be listed along with their operation type like transfer, update, withdraw, etc.,

<figure><img src="../../../.gitbook/assets/7-Fees.JPG" alt=""><figcaption><p>Fig-11: Fees details</p></figcaption></figure>



## 7. Accounts

This tab provides the list of accounts associated with the chain. Clicking on the account id/account name will direct to detailed activities of that particular account.

<figure><img src="../../../.gitbook/assets/Accounts-1.JPG" alt=""><figcaption><p>Fig-12 List of accounts</p></figcaption></figure>



<figure><img src="../../../.gitbook/assets/Accounts-activity.JPG" alt=""><figcaption><p>Fig-13: Activities of the account</p></figcaption></figure>

