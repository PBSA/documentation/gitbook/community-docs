---
description: A brief overview of the Peerplays decentralized exchange.
---

# Peerplays DEX

## 1. Decentralized Asset Exchanges

In the cryptocurrency space, a decentralized exchange (DEX) is a place to trade crypto assets while maintaining ownership of your private keys and thereby keeping control of your assets. There is no third party or broker in the DEX. Instead, the trading is managed by an impartial (and therefore trustworthy) automatic market-making algorithm.

## 2. The Peerplays DEX

The user can perform the following tasks using Peerplays DEX:

* use it as a wallet to manage your assets.
* monitoring the market activity of any trading pair.
* swap assets using the Peerplays PPY.&#x20;
* cast votes and participate in blockchain governance.
* manage your Peerplays account.
* register your Bitcoin, Hive, and Ethereum accounts to send and receive BTC, HIVE, and ETH.

## 3. Peerplays account

The user has to log in to Peerplays DEX to manage the Peerplays account. The user can monitor activities such as selling/buying assets, monitoring market activities, order history, profile setting, and voting in their account. Use the below document to create one,

{% embed url="https://peerplays.gitbook.io/peerplays-infrastructure-docs/~/changes/0IXXeM79c2vnSTcZBjuH/the-basics/how-to-create-a-peerplays-account" %}

## 4. Peerplays wallet

There is a built-in wallet for each account through which the user can trade the asset, receive the asset from BTC/HIVE/ETHEREUM, and maintain ownership. &#x20;

#### Deposits and withdrawals

You can deposit Bitcoin and Hive to your Peerplays wallet to use in the Peerplays DEX. Peerplays SONs (Sidechain Operator Nodes) enable off-chain assets to be deposited and withdrawn from Peerplays accounts. More chains will be added over time to bring the most popular assets to Peerplays.

When these assets are on the Peerplays chain, they benefit from the 3-second blocks of the Peerplays network. They can be traded, swapped, or staked like any other Peerplays asset. And they can be withdrawn back to their original chains as well. When these off-chain assets are on the Peerplays chain, they are always backed by an equal amount on their original chain.

#### Sending assets

Assets that you own can easily be sent to any other Peerplays account. When on the Peerplays chain, Bitcoin and Hive can be sent to other Peerplays accounts or addresses on their original chains.

## 5. Asset staking (PowerUp)

Staking your assets using the PowerUp option in the DEX will earn you rewards and voting power over time. Your staked assets supply liquidity pools used for asset swapping. In return, you'll receive an NFT that represents and tracks your stake in the liquidity pools.

The NFT reaches maturity when its locking period (which you choose) expires. At this point, you can PowerDown the NFT to retrieve your assets from the pool. At any time along the way, you can claim the rewards that have been accruing based on the size of your stake and the length of the locking period you chose. But beware, powering down the NFT means losing the voting power that has also been accruing.

Another option you have is to sell the NFT in the NFT marketplace. Especially if you have an aged NFT, you can fetch a premium price for your NFT.

## 6. GPOS - Voting

Transfer the PPY to GPOS balance to participate in the voting for best witnesses, advisors, SONS, and proposals. To increase the participation rewards, the user has to transfer more PPY into the GPOS balance and share Peerplays with others. Peerplays remains the most secure provably fair blockchain globally with Decentralized Autonomous Cooperative (DAC).&#x20;

Using Powerup, the user can participate in the DAC with which the user can become a big part globally, earn participant rewards, stake PPY while participating, bragging rights, and help secure Peerplays Blockchain.

## 7. Asset swapping

From the Peerplays account, using PPY asset swapping can be done with ease. The swapping mechanism is simple and quick compared with the traditional order book exchange. The exchange rate is calculated based on the available supply of each asset rather than set by traders seeking the best price.

## 8. The exchange

The Peerplays DEX provides a decentralized order book trading experience for those who prefer to set their own prices on trades. The DEX supports market, limit, and stop-limit orders. The exchange is where you can speculatively trade various markets. Since the Peerplays DEX keeps your private keys in your hands and is both a decentralized exchange and your wallet, there's no need to store your assets off the DEX. This means you can always be ready to trade in any market condition without the worry of hackers, exit scams, government takeovers, or bankruptcies (like what happens to centralized exchanges.)

## 9. Blockchain governance

The DEX is the place to cast your votes on important blockchain issues. You can help direct the blockchain and strengthen it by voting for the best node operators. Votes will also occur for setting fees for all the blockchain operations, how many node operators should run the network, which node operators are active, and network proposals. The voting happens on a continuous cycle so the blockchain can grow in the direction the voters wish to take it.

