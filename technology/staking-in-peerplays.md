# Staking (PowerUp) in Peerplays

![](../.gitbook/assets/nft-staking.png)

## Downloads

The Diagram in PDF form to easily view and share:

{% file src="../.gitbook/assets/nft-staking.pdf" %}
Staking (PowerUp) in Peerplays - PDF
{% endfile %}

The Diagram in Draw.io's XML format. You can edit the diargam using this file in Draw.io:

{% file src="../.gitbook/assets/nft-staking.xml" %}
Staking (PowerUp) in Peerplays - XML
{% endfile %}
