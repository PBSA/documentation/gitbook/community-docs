# Wallet User Guide

Version 1.5 of the Peerplays Core Wallet added the GPOS functionality for the first time; this document will step you through the new features it implements to ensure you qualify for maximum participation rewards.

{% content-ref url="gpos-panel.md" %}
[gpos-panel.md](gpos-panel.md)
{% endcontent-ref %}

{% content-ref url="gpos-landing-page.md" %}
[gpos-landing-page.md](gpos-landing-page.md)
{% endcontent-ref %}

{% content-ref url="power-up.md" %}
[power-up.md](power-up.md)
{% endcontent-ref %}

{% content-ref url="power-down.md" %}
[power-down.md](power-down.md)
{% endcontent-ref %}

{% content-ref url="vote.md" %}
[vote.md](vote.md)
{% endcontent-ref %}

{% content-ref url="thank-you-for-voting.md" %}
[thank-you-for-voting.md](thank-you-for-voting.md)
{% endcontent-ref %}

