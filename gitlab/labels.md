# Labels

[https://gitlab.com/groups/PBSA/-/labels](https://gitlab.com/groups/PBSA/-/labels)

## Scoped Labels

The scoped labels (ending with ::) are mutually exclusive. Only one of these labels can be assigned at a time to an issue.

### Priority::

* low&#x20;
* medium priority
* high priority
* critical priority

### State::

* pending&#x20;
* accepted
* in progress
* in review
* in testing
* completed
* on hold
* blocked
* revision needed

### Type::

* bug
* feature
* documentation
* question
* maintenance&#x20;

## Other Labels

* duplicate
* discussion
* security

## Issue Lifecycle

All issues should have a `state` and `type` scoped label attached to them. The following is a proposed lifecycle an issue should have:

1. Pending - Issues that are newly created and haven't been reviewed.
2. Accepted - Issues that are newly created and have been accepted after review.
3. In Progress - Issues that are currently being worked on.
4. In Review - Issues that require a proposed solution to be reviewed.
5. In Testing - Issues that require a proposed solution to be tested.&#x20;
6. Completed - Issues that are closed with a working solution.

This will cover a standard lifecycle of an issue. There are other `state` labels for issues that are not being worked on:

* On Hold - Issues that were at least accepted, that should not be worked on at the moment.
* Blocked - Issues that cannot be worked on due to other factors linked to it.
* Revision Needed - Issues that need to be revised before being accepted.

## Glossary

| Label                       | Description                                                                           |
| --------------------------- | ------------------------------------------------------------------------------------- |
| <p></p><p>priority::low</p> | (optional) Issues that are marked as low priority.                                    |
| priority::medium            | (optional) Issues that are marked as medium priority.                                 |
| priority::high              | (optional) Issues that are marked as high priority.                                   |
| priority::critical          | (optional) Issues that are marked as critical priority. Should be taken care of ASAP. |
| state::pending              | Issues that are newly created and haven't been reviewed.                              |
| state::accepted             | Issues that are newly created and have been accepted after review.                    |
| state::in progress          | Issues that are currently being worked on.                                            |
| state::in review            | Issues that require a proposed solution to be reviewed.                               |
| state::in testing           | Issues whose proposed solution is being tested.                                       |
| state::completed            | Issues that are closed with a working solution.                                       |
| state::on hold              | Issues that were at least accepted, that should not be worked on at the moment.       |
| state::blocked              | Issues that cannot be worked on due to other factors linked to it.                    |
| state::revision needed      | Issues that need to be revised before being accepted.                                 |
| type::bug                   | Issues that are an unexpected, or incorrect result in the project.                    |
| type::feature               | Issues that bring new functionality to the project.                                   |
| type::documentation         | Issues that relate to the documentation of the project.                               |
| type::question              | Issues that are questions related to the project.                                     |
| type::maintenance           | Issues that require maintenance on the project.                                       |
| duplicate                   | (optional) Issues that are duplicates of existing issues in the same project.         |
| discussion                  | (optional) Issues that discuss specific project changes.                              |
| security                    | (optional) Issues that relate to the security of the project.                         |

